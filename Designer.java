public class Designer  extends  Employee{
    String name;
    String surname;
    String salary;

    @Override
    public void work(){
        System.out.println("Creating site's design...");
    }

    public  Designer(String name, String surname, String salary){
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }
}
