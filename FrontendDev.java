public class FrontendDev extends Employee {
    int ID;
    String name;
    String surname;
    String salary;

    @Override
    public void work(){
        System.out.println("Creating the page..");
    }

    public  FrontendDev(String name, String surname, String salary){
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }
}
