import java.sql.*;

    public class DbWorker {

        Connection con = null;
        ResultSet rs = null;
        Statement stmt = null;

        public void  create(int ID, String Name, String Surname, String Salary){
            try {
                String sql = "INSERT INTO employees (id, name, surname, salary) "
                        + "VALUES (?, ?, ? , ?);";
                PreparedStatement Statement = con.prepareStatement(sql);
                Statement.setInt(1, ID);
                Statement.setString(2, Name);
                Statement.setString(3, Surname);
                Statement.setString(4, Salary);
                Statement.executeUpdate();

            }
            catch (Exception e) {
                e.printStackTrace();
                System.err.println(e.getClass().getName()+": "+e.getMessage());
                System.exit(0);
            }
        }

        public void total(String sum)throws SQLException{
            stmt.executeUpdate("INSERT INTO employees (salary) " + "VALUES ('total')");
            String sql = "INSERT INTO employees (salary) " + "VALUES (?);";
            PreparedStatement Statement = con.prepareStatement(sql);
            Statement.setString(1, sum);
        }


        public void read()  {
            try {


                rs = stmt.executeQuery("SELECT * FROM employees");


                while(rs.next()){
                    System.out.println(rs.getInt("id") + " " + rs.getString("name") +
                            " " + rs.getString("surname") + " " + rs.getString("salary"));
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                System.err.println(e.getClass().getName()+": "+e.getMessage());
                System.exit(0);
            }

        }

        public void Update(int ID, String Name, String Surname, String Salary) throws SQLException{
            String sql = "update employees set name=? , surname=?, salary = ? where id=?";
            PreparedStatement Statement = con.prepareStatement(sql);
            Statement.setString(1, Name);
            Statement.setString(2, Surname);
            Statement.setString(3, Salary);
            Statement.setInt(4, ID);
            Statement.executeUpdate();
        }

        public void delete(int ID) throws SQLException{
            String sql = "DELETE from employees where id = ?;";
            PreparedStatement Statement = con.prepareStatement(sql);
            Statement.setInt(1, ID);
            Statement.executeUpdate();
        }



        public DbWorker(){
            try {
                Class.forName("org.postgresql.Driver");
                con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/firstdb", "postgres", "Seven");
                stmt = con.createStatement();
                String sql = "CREATE TABLE employees " + "(id INT," +
                        " name           VARCHAR(15)," +
                        " surname        VARCHAR(15)," +
                        "salary           VARCHAR(6))";
                stmt.executeUpdate(sql);
            }
            catch (Exception e) {
                e.printStackTrace();
                System.err.println(e.getClass().getName()+": "+e.getMessage());
                System.exit(0);
            }
        }
    }

