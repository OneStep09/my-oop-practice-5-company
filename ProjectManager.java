public class ProjectManager extends Employee {
    int ID;
    String name;
    String surname;
    String salary;

    @Override
    public void work(){
        System.out.println("Dividing tasks..");
    }

    public  ProjectManager(String name, String surname, String salary){
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

}
