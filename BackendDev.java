public class BackendDev extends Employee {
    String name;
    String surname;
    String salary;

    @Override
    public void work(){
        System.out.println("Writing logic of the site...");
    }
    public  BackendDev(String name, String surname, String salary){
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }
}


