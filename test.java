import java.sql.SQLException;

public class test {
    public static void main(String[] args) throws SQLException {
        DbWorker db = new DbWorker();
        ProjectManager pm = new ProjectManager("Alex", "Smith", "5000$");
        FrontendDev frontendDev = new FrontendDev("Jhon", "Doe", "4000$");
        Designer designer = new Designer("Meghan", "Cameron", "4000$");
        BackendDev backendDev = new BackendDev("Jesse", "Green", "4500$");
        QA qa = new QA("Tony", "Wings", "4000$");

        pm.work();
        designer.work();
        frontendDev.work();
        backendDev.work();
        qa.work();

        db.create(1, pm.name, pm.surname, pm.salary);
        db.create(2, frontendDev.name, frontendDev.surname, frontendDev.salary);
        db.create(3, designer.name, designer.surname, designer.salary);
        db.create(4, backendDev.name, backendDev.surname, backendDev.salary);
        db.create(5, qa.name, qa.surname, qa.salary);
        db.read();



       int total = Integer.parseInt(pm.salary.substring(0, 4));
       total += Integer.parseInt(frontendDev.salary.substring(0, 4));
       total += Integer.parseInt(designer.salary.substring(0, 4));
       total += Integer.parseInt(backendDev.salary.substring(0, 4));
       total += Integer.parseInt(qa.salary.substring(0, 4));

       System.out.println("Total cost " + total);
    }

}



