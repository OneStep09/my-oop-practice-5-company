public class QA extends Employee {
    String name;
    String surname;
    String salary;

    @Override
    public void work(){
        System.out.println("Testing how the site works...");
    }

    public  QA(String name, String surname, String salary){
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }
}
